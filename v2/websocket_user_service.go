package binance

import (
	"encoding/json"
)

type WsExecutionReportEvent struct {
	EventType		string	`json:"e"`
	EventTime		int64	`json:"E"`
	OrderNewTime	int64	`json:"O"`
	Symbol			string	`json:"s"`
	ClientOrderId	string	`json:"c"`
	OrderId			int64	`json:"i"`
	Side			SideType	`json:"S"`
	OrderType		OrderType	`json:"o"`
	TimeInForce		TimeInForceType	`json:"f"`
	OrigQuantity	string	`json:"q"`
	OrigPrice		string	`json:"p"`
	OrderStatus		OrderStatusType	`json:"X"`
	ExecutedQty		string	`json:"z"`
	CummuQuoteQty	string	`json:"Z"`
	CommissionQty	string	`json:"n"`
	CommissionSymbol	string	`json:"N"`
}

type WsExecutionReportHandler func(event *WsExecutionReportEvent)

func WsExecutionReportServe(listenKey string, handler WsExecutionReportHandler, errHandler ErrHandler) (doneC, stopC chan struct{}, err error) {
	wsHandler := func(message []byte) {
		event := new(WsExecutionReportEvent)
		err := json.Unmarshal(message, &event)
		if err != nil {
			errHandler(err)
			return
		}
		if event.EventType != "executionReport" {
			return
		}
		handler(event)
	}
	return WsUserDataServe(listenKey, wsHandler, errHandler)
}